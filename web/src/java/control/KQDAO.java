/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Model.DB;
import Model.KQ;
import Model.TD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author cuong
 */
public class KQDAO {

    Connection conn;

    public KQDAO() {
        GetDB g = new GetDB();
        conn = g.getDB();
    }

    public boolean capnhatKQ(KQ kq) {
        boolean check = false;
        String sql = "SELECT * FROM tblKQ WHERE idTD=? AND idDB=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, kq.getTd().getId());
            ps.setInt(2, kq.getDb().getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                check = true;
            }
            if (check) {
                sql = "UPDATE tblKQ SET tySo=? WHERE idTD=? AND idDB=?";
                PreparedStatement ps1 = conn.prepareStatement(sql);
                ps1.setString(1, kq.getTs());
                ps1.setInt(2, kq.getTd().getId());
                ps1.setInt(3, kq.getDb().getId());
                ps1.executeUpdate();
            } else {
                sql = "INSERT INTO tblKQ (idTD,idDB,tySo) VALUES(?,?,?)";
                PreparedStatement ps2 = conn.prepareStatement(sql);
                ps2.setInt(1, kq.getTd().getId());
                ps2.setInt(2, kq.getDb().getId());
                ps2.setString(3, kq.getTs());
                ps2.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return true;
    }
    
    public ArrayList<KQ> getAllKQ(){
        String sql="SELECT * FROM tblKQ";
        ArrayList<KQ>list=new ArrayList<>();
        try{
            PreparedStatement ps=conn.prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
                int id=rs.getInt(1);
                int idTD=rs.getInt(2);
                TD td=new TD();
                td.setId(idTD);
                int idDB=rs.getInt(3);
                DB db=new DB();
                db.setId(idDB);
                String ts=rs.getString(4);
                KQ kq=new KQ(id,td, db, ts);
                list.add(kq);
            }
        }catch(SQLException ex){
            
        }
        return list;
    }
    public static void main(String[] args) {
        KQDAO dao=new KQDAO();
        ArrayList<KQ>list=dao.getAllKQ();
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i).getTd().getId() +" "+list.get(i).getDb().getId() +" "+list.get(i).getTs());
        }
    }
}
