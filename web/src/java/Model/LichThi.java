/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author coi_louis
 */
public class LichThi {
    private String doinha;
    private String doikhach;
    private String ngay; //nam-thang-ngay
    private String gio;
    private String time;
    
    public LichThi(String doinha, String doikhach, String ngay, String gio, String time) {
        this.doinha = doinha;
        this.doikhach = doikhach;
        this.ngay = ngay;
        this.gio = gio;
        this.time = time;
    }

    public String getDoinha() {
        return doinha;
    }

    public void setDoinha(String doinha) {
        this.doinha = doinha;
    }

    public String getDoikhach() {
        return doikhach;
    }

    public void setDoikhach(String doikhach) {
        this.doikhach = doikhach;
    }

    public String getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) {
        this.ngay = ngay;
    }

    public String getGio() {
        return gio;
    }

    public void setGio(String gio) {
        this.gio = gio;
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }
    
}
