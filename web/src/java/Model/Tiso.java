/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author coi_louis
 */
public class Tiso {
    private String doinha;
    private String doikhach;
    private String tiso;
    private String status;

    public Tiso(String doinha, String doikhach, String tiso, String status) {
        this.doinha = doinha;
        this.doikhach = doikhach;
        this.tiso = tiso;
        this.status = status;
    }

   

    public String getDoinha() {
        return doinha;
    }

    public void setDoinha(String doinha) {
        this.doinha = doinha;
    }

    public String getDoikhach() {
        return doikhach;
    }

    public void setDoikhach(String doikhach) {
        this.doikhach = doikhach;
    }

    public String getTiso() {
        return tiso;
    }

    public void setTiso(String tiso) {
        this.tiso = tiso;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
