/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author cuong
 */
public class KQ implements Serializable{
    private int id;
    private TD tddddddddd;
    private DB db;
    private String ts;

    public KQ() {
    }

    public KQ(TD td, DB db, String ts) {
        this.td = td;
        this.db = db;
        this.ts = ts;
    }

    public KQ(int id, TD td, DB db, String ts) {
        this.id = id;
        this.td = td;
        this.db = db;
        this.ts = ts;
    }

    public int getId() {
        return id;
    }

    public TD getTd() {
        return td;
    }

    public DB getDb() {
        return db;
    }

    public String getTs() {
        return ts;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTd(TD td) {
        this.td = td;
    }

    public void setDb(DB db) {
        this.db = db;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
    
    
}
