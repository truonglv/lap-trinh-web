/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import Model.LichThi;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author coi_louis
 */
public class API_LichThiDau {

    public ArrayList<LichThi> getAllLichThi() throws Exception{
        ArrayList<LichThi> list = new ArrayList<>();
        String key = "J3KAAiRoEj1k3wPY";
        String secret = "mS31VlDmBPfKnma6pVHZdyoSSpdVV2Ui";
        HttpClient client = HttpClientBuilder.create().build();
        //lay ti so
        String theUrl = "http://livescore-api.com/api-client/fixtures/matches.json?key=" + key
                + "&secret=" + secret + "&country=" + 84;

        HttpGet request = new HttpGet(theUrl);

        String line;

        HttpResponse response = client.execute(request);

        HttpEntity resEntityGet = response.getEntity();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                resEntityGet.getContent()));

        JSONParser parser = new JSONParser();

        while ((line = in.readLine()) != null) {
            Object obj = parser.parse(line);
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject datas = (JSONObject) jsonObject.get("data");
            JSONArray matchs = (JSONArray) (datas.get("fixtures"));

            for (Object match : matchs) {
                JSONObject m = (JSONObject) parser.parse(match.toString());
                
                String doinha = m.get("home_name").toString().trim().split(" ")[0];
                String doikhach = m.get("away_name").toString().trim().split(" ")[0];
                
                LichThi l = new LichThi(doinha,doikhach,
                        m.get("date").toString().trim(),
                        m.get("time").toString().trim(), "");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = dateFormat.parse(l.getNgay()+" "+l.getGio());
                
                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                cal.setTime(date);
                cal.add(Calendar.HOUR_OF_DAY, 6); // this will add 6 hours
                
                date = cal.getTime();
                l.setTime(date.toString().substring(0, 16));

                list.add(l);
            }
        }
        return list;
    }
   
}
// token sportmonks
//VmovjgHcS5U81Uc1Bs1UEa266bOlFlN3Y1pX7noA7pO6Y6WfRmp2jNTcrPx5