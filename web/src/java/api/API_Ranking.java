/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import Model.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author coi_louis
 */
public class API_Ranking {
    
    public ArrayList<Player> getList(int code) throws Exception {
        ArrayList<Player> list = new ArrayList<>();
        URL url = new URL("http://utils1.cnnd.vn/apibangxephang.ashx?c=" + code);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
        String inputLine;
        JSONParser parser = new JSONParser();
          
        while ((inputLine = in.readLine()) != null) {
            JSONArray jarr = (JSONArray) parser.parse(inputLine);
            for (int i = 0; i < jarr.size(); i++) {
                JSONObject obj = (JSONObject) parser.parse(jarr.get(i).toString());
                int hs = Integer.parseInt(obj.get("BT").toString().trim())
                        - Integer.parseInt(obj.get("BB").toString().trim());
                Player player = new Player(obj.get("Doi").toString(),
                                Integer.parseInt(obj.get("SoTran").toString().trim()),
                                Integer.parseInt(obj.get("D").toString().trim()), hs);
         
                list.add(player);
            }
        }
        in.close();
        return list;
    }
}
