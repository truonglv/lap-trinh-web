/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import Model.Tiso;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author coi_louis
 */
public class API_Tiso {

    public ArrayList<Tiso> getTiso() throws Exception {
        ArrayList<Tiso> list = new ArrayList<>();
        String key = "J3KAAiRoEj1k3wPY";
        String secret = "mS31VlDmBPfKnma6pVHZdyoSSpdVV2Ui";
        HttpClient client = HttpClientBuilder.create().build();
        //lay ti so
        String theUrl = "http://livescore-api.com/api-client/scores/live.json?key=" + key
                + "&secret=" + secret +"&country="+67;

        HttpGet request = new HttpGet(theUrl);

        String line;

        HttpResponse response = client.execute(request);

        HttpEntity resEntityGet = response.getEntity();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                resEntityGet.getContent()));

        JSONParser parser = new JSONParser();

        while ((line = in.readLine()) != null) {
            Object obj = parser.parse(line.toString());
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject datas = (JSONObject) jsonObject.get("data");
            JSONArray matchs = (JSONArray) (datas.get("match"));
           
            for (Object match : matchs) {
                 JSONObject m = (JSONObject) parser.parse(match.toString());
                Tiso tiso = new Tiso(m.get("home_name").toString(), m.get("away_name").toString(),
                                m.get("score").toString(),m.get("status").toString());
                list.add(tiso);
            }
        }
        return list;
    }
}
