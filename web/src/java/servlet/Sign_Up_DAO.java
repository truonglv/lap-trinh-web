/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author truonglv
 */
public class Sign_Up_DAO {
    public Connection con = null;
    
    public Sign_Up_DAO(){
        String dbCLass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String url = "jdbc:sqlserver://localhost;"
                + "databaseName=LTM;user=sa;password=12345678;";
        try {
            Class.forName(dbCLass);
            con = DriverManager.getConnection(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean check(String user){
        String sql = "select * from tblAccount where username = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public boolean add(String user, String pass, String email){
        String sql = "insert into tblAccount(username, password, email) "
                + "values(?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.setString(3, email);
            ps.executeUpdate();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
