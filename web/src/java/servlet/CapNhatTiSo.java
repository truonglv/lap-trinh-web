/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Dao.KQDAO;
import Model.DB;
import Model.KQ;
import Model.TD;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cuong
 */
public class CapNhatTiSo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session=request.getSession();
        int vt=(int) session.getAttribute("vtsua");
        int vtTD=Integer.parseInt(request.getParameter("idTD"));
        ArrayList<TD> allTD = (ArrayList<TD>) session.getAttribute("allTD");
        DB db=(DB) session.getAttribute("doibongdang");
        String tmp1=request.getParameter("ts1");
        String tmp2=request.getParameter("ts2");
        int ts1=0,ts2=0;
        String kq="";
        try{
            ts1=Integer.parseInt(tmp1);
            ts2=Integer.parseInt(tmp2);
            if(db.getId()==allTD.get(vtTD).getDb1().getId()){
                kq=ts1+"-"+ts2;
            }else{
                kq=ts2+"-"+ts1;
            }
            KQ ts=new KQ(allTD.get(vtTD), db, kq);
            KQDAO dao=new KQDAO();
            dao.capnhatKQ(ts);
        }catch(NumberFormatException ex){
        }
        response.sendRedirect("List_Member.jsp?vt="+vt);
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CapNhatTiSo</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CapNhatTiSo at " + allTD.size()+""+vtTD+"</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
