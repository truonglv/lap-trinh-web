<%-- 
    Document   : SignUp
    Created on : Dec 3, 2017, 4:38:31 PM
    Author     : truonglv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="SignUp.css">
        <script type="text/javascript">
            function checkEmail() {
                var email = document.getElementById('email');
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(email.value)) {
                    alert('Hay nhap dia chi email hop le.\nExample@gmail.com');
                    email.focus;
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="content">
            <form class="form-signin" action="Sign_up" method="POST">
                <h2>Sign up</h2>

                <input class="form-control" id="email" name="email" placeholder="Email" type="text" required>
                <input class="form-control" name="user" placeholder="User name" type="text" required >
                <input class="form-control" name="pass" placeholder="Password" type="password" required >
                <br><br>
                <input type="submit" class="button" onclick="checkEmail()" value="Sign Up">     
                <div>

                </div>

                <small>Connect [your service] with your favorite social network</small>
                <a href="https://www.facebook.com/r.php?locale=vi_VN&display=page" class="connect facebook">
                    <div class="connect__context">
                        <span>Sign up with <strong>Facebook</strong></span>
                    </div>
                </a>

                <a href="https://accounts.google.com/SignUp?hl=vi" class="connect googleplus">
                    <div class="connect__context">
                        <span>Sign up with <strong>Google+</strong></span>
                    </div>
                </a>
            </form>
        </div>

    </body>
</html>
