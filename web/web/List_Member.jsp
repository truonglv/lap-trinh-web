<%@page import="Model.KQ"%>
<%@page import="Dao.KQDAO"%>
<%@page import="Model.TD"%>
<%@page import="Model.DB"%>
<%@page import="Model.CT"%>
<%@page import="Dao.DBDAO"%>
<!DOCTYPE html>
<%@page language="java" import = " java.util.*, java.awt.*" %> 
<%
    int vt = Integer.parseInt(request.getParameter("vt"));
    ArrayList<DB> listDB = (ArrayList<DB>) session.getAttribute("allDB");
    CT user = (CT) session.getAttribute("user");
    DB db = listDB.get(vt);
    session.setAttribute("doibongdang", db);
    session.setAttribute("vtsua", vt);
    ArrayList<TD> allTD = (ArrayList<TD>) session.getAttribute("allTD");
    session.setAttribute("allTD", allTD);
    KQDAO KQdao = new KQDAO();
    ArrayList<KQ> listKQ = KQdao.getAllKQ();
%>
<html>
    <%@page contentType="text/html" pageEncoding="utf-8"%>
    <head>
        <title>List Member</title>
        <link rel="stylesheet" type="text/css" href="CSS/Manager_team.css">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script src="Script/Script.js" type="text/javascript"></script>	

    </head>

    <body>
        <div id = "Top">
            <div id ="Top_Menu">
                <div id = "Top_Left">

                    <div id = "Top_Left_Logo">
                        <img src ="Image/logo.png" width="30px" height="30px">
                    </div>
                    <div id ="Top_Left_Search">
                        <form id ="Top_Left_Search_Form">
                            <input type ="text" placeholder ="Tìm kiếm..." id = "Top_Left_Search_TextSearch">
                            <button type= "submit" id ="Top_Left_Search_ButtonSearch"></button>
                        </form>
                    </div>
                </div>

                <div id = "Top_Right">
                    <div class = "Top_Right_Tool">
                        <ul>
                            <li><a href="Home.jsp">Trang chủ</a></li>
                            <li>|</li>
                        </ul>
                    </div>
                    <div id = "Top_Right_User">
                        <a href="Profile.jsp?idct=<%=user.getId()%>">
                            <div id = "image_user">

                                <img src ="Image/<%=user.getLinkAnh()%>" width ="25px" height ="25px" style="border-radius: 50%" />
                            </div>
                            <div id = "ten_user"><%=session.getAttribute("tenuser")%></div>
                            <img src ="Image/Desc.png"width ="23px" height ="24px" />
                            <ul> 
                                <li><a href = "TaoDoiBong.jsp">Tạo đội</a></li>
                                <br/>
                                <hr/>
                                <li><a href = "#">Đăng xuất</a></li>
                            </ul>
                        </a>
                    </div>

                </div>
            </div>

        </div>
        <div id = "website"> 


            <div id="manager_left">
                <div id="manager_left_0">
                    <button id = "mouse" onclick="myclick()">
                        <img src="Image/x_logo.png" style="width:10px;height: 10px">
                    </button>
                    <img alt="anhdaidien" src="Image/<%=db.getLinkLogo()%>">
                </div>
                <div id="tendoi"><a href="#"><%=db.getTendoi()%></a></div>
                <div class="manager_left_1">
                    <p>Lối tắt</p>
                    <ul id="doibong">
                        <% for (int i = 0; i < listDB.size(); i++) {%>
                        <li><img src="Image/<%=listDB.get(i).getLinkLogo()%>" width="28px" height="28px" style="border-radius: 50%; border: 1px solid #CCCCCC">
                            <a href="List_Member.jsp?vt=<%=i%>"><%=listDB.get(i).getTendoi()%></a>
                        </li>
                        <% }%>
                        <!--                        <li style="margin-top: 5px"><input type="submit" id="themdoibongmoi" onclick="hienthongtin()" value="Thêm đội bóng"></li>        -->
                        <li><a href = "TaoDoiBong.jsp">Thêm đội bóng</a></li>
                    </ul>
                </div>
            </div>
            <div id="manager_center">
                <div id="manager_center_header">
                    <p>Thông tin liên hệ: <%=user.getTenCT()%><br/>
                        Số điện thoại:    <%=user.getSdt()%><br/>
                        Ngày thành lập:   <%=db.getNgayTL()%>
                    </p>
                </div>
                <div id="manager_center_nav">
                    <ul> 
                        <li><input type="submit" id="clicktimdoithu" onclick="timdoithu()" value="Bảng tin"></li>
                        <li><input type="submit" id="clickok" onclick="myFunction()" value="Danh sách" for></li>
                        <li><input type="submit" id="clickloccauthu" onclick="loccauthu()" value="Lọc cầu thủ"></li>
                        <li><input type="submit" id= "clicksuathongtin" onclick="hienthongtindoibong()" value="Sửa thông tin"></li>
                        <li><input type="submit" id= "clickxoadoibong" onclick="myclick1()" value="Giải tán" ></li>
                    </ul>
                    <!--                    <input type="submit" id="clickok" onclick="myFunction()" value="Xem chi tiết ảnh">
                                        <input type="submit" id= "clicksuathongtin" onclick="hienthongtindoibong()" value="Sửa thông tin đội bóng ">
                                        <input type="submit" id="clickloccauthu" onclick="loccauthu()" value="Lọc cầu thủ">
                                        <input type="submit" id="clicktimdoithu" onclick="timdoithu()" value="Tìm đối">
                                        <form action="../xoaDoiBong" method="POST" style="float: left">
                                            <input type="text" name="iddb" value="" style="display: none;">
                                            <input type="submit" id= "clickxoadoibong" onclick="myclick1()" value="Giải tán đội bóng" >
                                        </form>-->

                </div>
                <div id="manager_center_nav_danhsach">

                    <div id="camnghi">
                        <img src="Image/<%=db.getLinkLogo()%>" width="28px" height="28px" style="border-radius: 50%; border: 1px solid #CCCCCC; float: left">
                        <p style="font-size: 15px; padding-left: 20px"><strong><%=db.getTendoi()%> </strong> CẦN TÌM ĐỐI THỦ...</p>
                        <form method="POST" action="DangBai">
                            <table style="margin: 20px " id="thongtinbaidang">
                                <tr>
                                    <td><img src ="Image/ThoiGian.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Thời gian: </strong></td>
                                    <td><input type="time" name="thoigian" required></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/ThoiGian.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Ngày: </strong></td>
                                    <td><input type="date" name="ngay" required></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/DiaDiem.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Địa điểm: </strong></td>
                                    <td><input type="text" name="diadiem" required></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/TyLe.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Tỷ lệ </strong></td>
                                    <td><input type="text" name="tyle" required></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/TyLe.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Nội dung </strong></td>
                                    <td><textarea rows="3" cols="40" name="noidung"></textarea></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/TyLe.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Hình ảnh </strong></td>
                                    <td><input type="file" name="anh" accept="Image/*"/></td>
                                </tr>
                            </table>


                            <footer id="footer1">
                                <hr style="color: #CCCCCC; margin-left: 20px; margin-right: 20px; margin-top: 20px">
                                <input type="submit" value="Đăng" style="float: right" id="dang">
                                <button onclick="hienthongtinbaidang()">Thông tin</button>
                            </footer>
                        </form>
                    </div>
                        
                    <div id="dstrandau">
                        <p>Trận đấu gần đây</p>
                        <%
                            for (int i = 0; i < allTD.size(); i++) {
                                if ((allTD.get(i).getDb1().getId() == db.getId() && allTD.get(i).getDb2().getId() != 0) || (allTD.get(i).getDb2().getId() == db.getId())) {
                                    String ts1 = "?", ts2 = "?";
                                    TD td = allTD.get(i);
                                    DBDAO dao = new DBDAO();
                                    DB db1 = dao.getDB(td.getDb1().getId());
                                    DB db2 = dao.getDB(td.getDb2().getId());
                                    for (int j = 0; j < listKQ.size(); j++) {
                                       if(listKQ.get(j).getTd().getId()==td.getId()){
                                           if(listKQ.get(j).getDb().getId()==db.getId()&&db.getId()==db1.getId()){
                                               String kq=listKQ.get(j).getTs();
                                               ts1=kq.substring(0,1);
                                               ts2=kq.substring(2, 3);
                                           }else if(listKQ.get(j).getDb().getId()==db.getId()&&db.getId()==db2.getId()){
                                               String kq=listKQ.get(j).getTs();
                                               ts2=kq.substring(0,1);
                                               ts1=kq.substring(2, 3);
                                               
                                           }
                                       }
                                    }
                        %>
                        <div class="trandau">
                            <td><p><%=td.getThoiGianTD()%></p></td>
                            <form action="CapNhatTiSo" method="POST">
                                <input type="text" value="<%=i%>" name="idTD" style="display: none">
                                <table>
                                    <tr>
                                        <td><img src="/Image/<%=db1.getLinkLogo()%>" width="28px" height="28px" style="border-radius: 50%; border: 1px solid #CCCCCC; float: left"/>
                                            <p style="font-size: 15px" ><strong><%=db1.getTendoi()%></strong></p>
                                        </td>
                                        <td><p><input type="text" name="ts1" size="1" placeholder="<%=ts1%>"> - <input type="text" name="ts2" size="1" placeholder="<%=ts2%>"></p></td>
                                        <td><img src="/Image/<%=db2.getLinkLogo()%>" width="28px" height="28px" style="border-radius: 50%; border: 1px solid #CCCCCC; float: left"/>
                                            <p style="font-size: 15px" ><strong><%=db2.getTendoi()%></strong></p>
                                        </td>
                                    </tr>
                                </table>
                                <input type="submit" value="Cập Nhật" onclick="tyso()">
                            </form>
                        </div>
                        <%}
                            }%>
                    </div>
                    <% for (int i = 0; i < allTD.size(); i++) {
                            if (allTD.get(i).getDb1().getId() == db.getId()) {
                                TD td = allTD.get(i);
                    %>
                    <div class="website_tintuc">

                        <div class="website_tintuc_logo">
                            <img src="/Image/<%=db.getLinkLogo()%>" width="28px" height="28px" style="border-radius: 50%; border: 1px solid #CCCCCC; float: left">
                            <p style="font-size: 15px" ><strong><%=db.getTendoi()%></strong> CẦN TÌM ĐỐI THỦ </p>
                        </div>
                        <hr style="color: #CCCCCC; margin-left: 20px; margin-right: 20px; margin-top: 20px">
                        <div class="website_tintuc_noidung">
                            <table style="margin-left: 60px; font-size: 20px;">
                                <tr>
                                    <td><img src ="Image/ThoiGian.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Thời gian </strong></td>
                                    <td><%=td.getThoiGianTD()%></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/DiaDiem.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Địa điểm </strong></td>
                                    <td><%=td.getDiaDiemTD()%></td>
                                </tr>
                                <tr>
                                    <td><img src ="Image/TyLe.png"width ="24px" height ="24px" /></td>
                                    <td><strong>Tỷ lệ </strong></td>
                                    <td><%=td.getTyLe()%></td>
                                </tr>
                            </table>
                            <hr style="color: #CCCCCC; margin-left: 20px; margin-right: 20px; margin-top: 20px">
                            <p style="margin: 20px; font-size: 20px;"> 
                                <%
                                    String s = td.getNoiDung();
                                %>
                                <%=s%>
                            </p>
                            <img src="Image/AnhDaiDienCuong.jpg" width="460px" height="400px" style="margin-left: 20px">
                        </div>

                    </div>
                    <%}
                        }%>

                    <div id="loccauthu">
                        <table style="margin: auto"> 
                            <br>
                            <tr>
                                <th>Quê quán</th>
                                <th>Vị trí</th>
                                <th>Nghề nghiệp</th>
                            </tr>
                            <tr>
                                <td>
                                    <select id="quequan">
                                        <option value="Nam dinh">Nam Dinh</option>
                                        <option value="Ha Noi">Ha Noi</option>
                                        <option value="Bac Giang">Bac Giang</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="vitri">
                                        <option value="Hau ve">Hau ve</option>
                                        <option value="Thu mon">Thu mon</option>
                                        <option value="Tien dao">Tien dao</option>
                                        <option value="Trung ve">Trung ve</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="nghenghiep">
                                        <option value="Sinh vien">Sinh vien</option>
                                        <option value="Giang Vien">Giang vien</option>
                                    </select>
                                </td>
                                <td>
                                    <button onclick="chonloc()">Loc</button>
                                </td>
                            </tr>
                        </table>
                        <br><br><hr>

                    </div>
                    <div id="thongtinsuadoibong">
                        <p style = "margin-top: 20px; color: fuchsia;">THÔNG TIN ĐỘI BÓNG</p>
                        <br>
                        <form action="SuaThongTinDoiBong" method="POST">
                            <table>
                                <tr>
                                    <td>Tên đội bóng</td>
                                    <td><input type="text" name="tendoibong" value="<%=db.getTendoi()%>" style="height: 20px" required></td>
                                </tr>
                                <tr> 
                                    <td>Ngày thành lập</td>
                                    <td><input type="text" name="ngaytl" value="<%=db.getNgayTL()%>" style="height: 20px" required></td>
                                </tr>
                                <tr>
                                    <td>Logo</td>
                                    <td><input type="file" name="logo" value="<%=db.getLinkLogo()%>" accept="Image/*" required/></td>
                                </tr>
                            </table>
                            <br>
                            <input type="text" name="iddb" value="" style="display: none;">
                            <input id="luuthongtinsuadoibong" type="submit" onclick="luuthongtinsuadoibong()" value="Lưu thông tin">
                            <br><br>

                        </form>
                    </div>
                    <div id="thongtindoibong" >
                        <div id="thongtindoibong">
                            <p style = "margin-top: 20px; color: fuchsia;">THÔNG TIN ĐỘI BÓNG</p>
                            <br>
                            <form action="../ThemDoiBongServlet" method="POST">
                                <table>
                                    <tr>
                                        <td>Tên đội bóng</td>
                                        <td><input type="text" name="tendoibong" style="height: 20px" Required></td>
                                    </tr>
                                    <tr> 
                                        <td>Ngày thành lập</td>
                                        <td><input type="text" name="ngaythanhlap" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Người đại diện</td>
                                        <td><input type="text" name="nguoidaidien" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Liên hệ</td>
                                        <td><input type="text" name="lienhe" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Logo</td>
                                        <td><input type="file" name="logo" accept="image/*"/></td>
                                    </tr>
                                </table>
                                <br>
                                <p style = "margin-top: 20px; color: fuchsia;">THÔNG TIN LIÊN HỆ</p>
                                <br>
                                <table>
                                    <tr> 
                                        <td>Vị trí sở trường</td>
                                        <td ><input type="text" name="vitri" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Số áo ưa thích</td>
                                        <td><input type="text" name="soao" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Thể hình</td>
                                        <td><input type="text" name="thehinh" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Ảnh đại diện</td>
                                        <td><input type="file" name="anhdaidien" name="chọn file" accept="image/*" /></td>
                                    </tr>
                                    <tr>
                                        <td>Ngày sinh*</td>
                                        <td><input type="text" name="ngaysinh" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Quê quán*</td>
                                        <td><input type="text" name="quequan" style="height: 20px" Required></td>
                                    </tr>
                                    <tr>
                                        <td>Nghề nghiệp*</td>
                                        <td><input type="text" name="nghenghiep" style="height: 20px" Required></td>
                                    </tr>
                                </table>
                                <input id="luuthongtin" type="submit" onclick=" alert('Them thanh cong!');" value="Lưu thông tin">
                            </form>
                        </div>
                    </div>
                    <div id = "thongtincauthu">
                        <br>
                        <p style>THÔNG TIN CƠ BẢN</p>
                        <table>
                            <tr>
                                <td>Họ tên*</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td>Ngày sinh*</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>

                            <tr>
                                <td>SĐT liên hệ*</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>
                        </table>
                        <br>
                        <p style = "margin-top: 20px">THÔNG TIN CHUYÊN MÔN</p>

                        <table>
                            <tr> 
                                <td>Vị trí sở trường</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td>Số áo ưa thích</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td>Thể hình</td>
                                <td><input type="text" style="height: 20px"></td>
                            </tr>
                        </table>
                        <form><input type="submit" id="clickluuthongtincauthuthem" value="Lưu thông tin"></form>
                    </div>
                    <% for (int i = 0; i < db.getAlCT().size(); i++) {%>
                    <a href="Profile.jsp?idct=<%=db.getAlCT().get(i).getId()%>">
                        <div class="thanhvien">
                            <h2><span><%=db.getAlCT().get(i).getTenCT()%></span>
                                <p><span><%=db.getAlCT().get(i).getSoAo()%></span></p></h2>
                            <img src = "Image/<%=db.getAlCT().get(i).getLinkAnh()%>">

                            <div class = "chiTiet">
                                <li><strong>Vị trí </strong><%= db.getAlCT().get(i).getViTri()%></li>
                                <li><strong>Ngày sinh </strong><%= db.getAlCT().get(i).getNgaySinh()%></li>
                                <li><strong>Thể hình </strong><%= db.getAlCT().get(i).getTheHinh()%></li>
                                <li><strong>Quê quán </strong><%= db.getAlCT().get(i).getQueQuan()%></li>
                                <li><strong>Nghề nghiệp </strong><%= db.getAlCT().get(i).getNgheNghiep()%></li>
                            </div>
                        </div></a>
                        <% }%>




                </div>

            </div>

        </div>
        <script>
            document.getElementById("thongtindoibong").style.display = "none";
            document.getElementById("thongtinsuadoibong").style.display = "none";
            document.getElementById("thongtincauthu").style.display = "none";
            document.getElementById("loccauthu").style.display = "none";
            var e = document.getElementsByClassName("thanhvien");
            for (var i = 0; i < e.length; i++) {
                e[i].style.display = "none";
            }
            document.getElementById("thongtinbaidang").style.display = "none";
        </script>
        <script>
            function hienthongtinbaidang() {
                document.getElementById("thongtinbaidang").style.display = "block";
            }
        </script>
    </body>   

</html>