<%-- 
    Document   : home
    Created on : Dec 22, 2017, 10:34:44 AM
    Author     : truonglv
--%>

<%@page import="java.util.Calendar"%>
<%@page import="api.API_Tiso"%>
<%@page import="Model.Tiso"%>
<%@page import="api.API_Ranking"%>
<%@page import="Model.Player"%>
<%@page import="java.util.ArrayList"%>
<%@page import="api.API_LichThiDau"%>
<%@page import="Model.LichThi"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="home.css">
        <meta charset="utf-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div>
            <p>toi yeu viet nam</p>
        </div>
        <div class="boss">
            <div class="lichthi">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Thời gian</th>
                            <th>Đội nhà</th>
                            <th>Đội khách</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            ArrayList<LichThi> list = new API_LichThiDau().getAllLichThi();
                            ArrayList<LichThi> listTow = new ArrayList<>();
                            Calendar cal = Calendar.getInstance();
                            String date = cal.getTime().toString().substring(0, 10);
                        %>

                        <tr>
                            <td><%="Ngày " + date%></td>
                        </tr>

                        <% for (LichThi lt : list) {
                                if (lt.getTime().substring(0, 10).equals(date)) {

                        %>
                        <tr>
                            <td><%= lt.getTime().substring(11)%></td>
                            <td><%= lt.getDoinha()%></td>
                            <td><%= lt.getDoikhach()%></td>
                        </tr>
                        <% }//end if
                                else
                                    listTow.add(lt);
                            }//end for
                            cal.add(Calendar.DAY_OF_YEAR, 1);
                            String tow = cal.getTime().toString().substring(0, 10);
                        %>
                        <tr>
                            <td><%="Ngày " + tow%></td>
                        </tr>
                        <%
                            for (LichThi l : listTow) {

                        %>
                        <tr>
                            <td><%= l.getTime().substring(11)%></td>
                            <td><%= l.getDoinha()%></td>
                            <td><%= l.getDoikhach()%></td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="master">
            <div class="rank">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tên đội</th>
                            <th>Số trận</th>
                            <th>Hệ số</th>
                            <th>Điểm</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            ArrayList<Player> rank = new API_Ranking().getList(3);
                            for (Player lt : rank) {

                        %>
                        <tr>
                            <td><%= lt.getTen()%></td>
                            <td><%= lt.getSotran()%></td>
                            <td><%= lt.getHeso()%></td>
                            <td><%= lt.getDiem()%></td>
                        </tr>
                        <% }%>
                    </tbody>
                </table>

            </div>
            <div class="bottom" >
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>Đội nhà</th>
                            <th>Tỉ số</th>
                            <th>Đội khách</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            ArrayList<Tiso> tiso = new API_Tiso().getTiso();
                            for (Tiso ts : tiso) {

                        %>
                        <tr>
                            <td><%= ts.getDoinha()%></td>
                            <td><%= ts.getTiso()%></td>
                            <td><%= ts.getDoikhach()%></td>
                        </tr>
                        <% }%>
                    </tbody>
                </table>
            </div>
        </div>


    </body>
</html>
