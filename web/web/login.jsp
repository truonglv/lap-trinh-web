<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Login</title>
        <meta name="google-signin-scope" content="profile email">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" 
              content="839997390899-echjbpgbd6lj33sc2n76grl0md7qfo9a.apps.googleusercontent.com">

        <link rel="stylesheet" type="text/css" href="login.css">

    </head>
    <body>
        <div class="content">      
            <form class="form-signin" action="Login" method="GET">
                <h2>Sign in</h2>
                <small>sign in with [your service]</small>

                <input class="form-control" id="user" name="user" placeholder="User name" type="text" required>
                <input class="form-control" name="pass" placeholder="Password" type="password" required>
                <br><br>
                <div>
                    <input type="submit" value="Sign In" class="button">
                    <small>Or</small>
                    <a href="SignUp.jsp" class="button">Sign Up</a>
                </div>
                <br><br>
                <small>Connect [your service] with your favorite social network</small>

                <br><br>
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                            return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=252032708661989';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-login-button" data-width="300" data-max-rows="1" data-size="large" data-button-type="login_with"></div>
            </form>
        </div>

        <script>

            function login() {
                FB.init({
                    appId: '252032708661989',
                    cookie: true, // enable cookies to allow the server to access 
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.11' // use version 2.2
                });
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));

                FB.login(function (response) {
                    if (response.authResponse) {
                        FB.api('/me?fields=name,id', function (response) {
                            console.log('Good to see you, ' + response.name + '.');
                        });
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                });
            }
            // This is called with the results from from FB.getLoginStatus().
            function statusChangeCallback(response) {
                if (response.status === 'connected') {
                    testAPI();
                } else if (response.status === 'not_authorized') {
                    document.getElementById('status').innerHTML = 'Login with Facebook ';
                } else {
                    document.getElementById('status').innerHTML = 'Login with Facebook ';
                }
            }
            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }
            function testAPI() {
                FB.api('/me?fields=name,id', function (response) {
                    document.getElementById("status").innerHTML = '<p>Welcome ' + response.name + " " + response.id + '</p>'
                });
            }
        </script> 
    </body>
</html>

<!--idFB - 252032708661989

<center>
    <div class="g-signin2" data-onsuccess="onSignIn" id="myP"></div>
    <img id="myImg"><br>
    <p id="name"></p>
    <div id="status">
    </div>

    login gmail
    <script type="text/javascript">
        function onSignIn(googleUser) {
        // window.location.href='success.jsp';
        var profile = googleUser.getBasicProfile();
        var imagurl = profile.getImageUrl();
        var name = profile.getName();
        var email = profile.getEmail();
        document.getElementById("myImg").src = imagurl;
        document.getElementById("name").innerHTML = name;
        document.getElementById("myP").style.visibility = "hidden";
        document.getElementById("status").innerHTML = 'Welcome ' + name + '!<a href=success_gmail.jsp?                  
                email = '+email+' & name = '+name+' / > Continue with Google login < /a></p >
        }
    </script>
    <button onclick="myFunction()">Sign Out</button>
    <script>
        function myFunction() {
        gapi.auth2.getAuthInstance().disconnect();
        location.reload();
        }
    </script>

//gmail 2 
<!--            <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
            <script>
                function onSignIn(googleUser) {
                    // Useful data for your client-side scripts:
                    var profile = googleUser.getBasicProfile();
                    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                    console.log('Full Name: ' + profile.getName());
                    console.log('Given Name: ' + profile.getGivenName());
                    console.log('Family Name: ' + profile.getFamilyName());
                    console.log("Image URL: " + profile.getImageUrl());
                    console.log("Email: " + profile.getEmail());
                    // The ID token you need to pass to your backend:
                    var id_token = googleUser.getAuthResponse().id_token;
                    console.log("ID Token: " + id_token);
                }
                ;
            </script>
        </div>-->
<!--end gmail 2-->

